import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'contain'
})
export class ContainPipe implements PipeTransform {

  transform(value: string, keyword: string): any {
    if (!keyword || new RegExp(keyword).test(value)) {
      return value;
    } else {
      return null;
    }
  }

}
