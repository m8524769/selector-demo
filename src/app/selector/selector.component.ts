import { Component, OnInit } from '@angular/core';
import { SelectorService } from './selector.service';
import { Division } from './division';

@Component({
  selector: 'app-selector',
  templateUrl: './selector.component.html',
  styleUrls: ['./selector.component.scss']
})
export class SelectorComponent implements OnInit {

  expanded: boolean = false;
  divisions: Division[] = [];
  current: Division[] = [];

  address: string[] = [];
  level: number = 0;

  keyword: string;

  constructor(
    private selectorService: SelectorService,
  ) { }

  ngOnInit() {
    this.getDivisions();
  }

  getDivisions(): void {
    this.selectorService.getDivisions()
      .subscribe(divisions => {
        this.divisions = divisions;
        this.current = divisions;
      });
  }

  select(index: number): void {
    this.address[this.level] = this.current[index].name;
    this.current = this.current[index].children;
    ++this.level;
    this.keyword = "";
  }

  clear(): void {
    this.address = [];
    this.current = this.divisions;
    this.level = 0;
  }

  switch(): void {
    this.expanded = !this.expanded;
  }

}
