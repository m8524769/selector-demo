export interface Division {
  code: string;
  name: string;
  children: Division[];
}
