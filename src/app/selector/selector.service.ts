import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Division } from './division';

@Injectable({
  providedIn: 'root'
})
export class SelectorService {

  divisionsUrl: string = "https://raw.githubusercontent.com/modood/Administrative-divisions-of-China/master/dist/pcas-code.json";

  constructor(
    private http: HttpClient,
  ) { }

  getDivisions(): Observable<Division[]> {
    return this.http.get<Division[]>(this.divisionsUrl);
  }
}
