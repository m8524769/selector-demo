import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SelectorComponent } from './selector.component';
import { ContainPipe } from './contain.pipe';

@NgModule({
  declarations: [
    SelectorComponent,
    ContainPipe,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    SelectorComponent,
  ]
})
export class SelectorModule { }
